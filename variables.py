### Tunna, we need to input sys id one by one by checking application list given by SNOW team. 
### In this example, I input sys id as value and app name as key. 
### To access the value, we need to get app name from gitlab-ci.yaml file and pass it to our tag.py
### tag.py will pass app name to this function pass_sys_id and receive sys_id back

## Variabilize System ID into a dictionary with application name as key and sys_id as value
def pass_sys_id(app_name):
    app_dic = {
        "ria_onboarding": "INC0000060"
    }
    return app_dic.get(app_name)
