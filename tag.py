#Need to install requests package for python
#easy_install requests
import requests
import json
import ruamel.yaml
#import variable
import os
from variables import pass_sys_id

### Get System ID. We will need to variabilie app name too (in this case it's ria_onboarding. We still hard coding here.)
### When variabilize app name, we will get actual app name from gitlab-ci.yaml with sed command
sys_id = pass_sys_id("ria_onboarding")

#number = variable.number

### Calling SNOW CI
url = f"https://dev134570.service-now.com/api/now/table/incident?sysparm_query=number%3D{sys_id}&sysparm_fields=number&sysparm_liit=1"

### Eg. User name="admin", Password="admin" for this code sample.
user = os.environ["user"]
pwd = os.environ["pwd"]

### Set proper headers
headers = {"Number":"INC0000060","Short description":"test","Content-Type":"application/json","Accept":"application/json"}

# Do the HTTP request
response = requests.get(url, auth=(user, pwd), headers=headers )

# Check for HTTP codes other than 200
if response.status_code != 200: 
    print('Status:', response.status_code, 'Headers:', response.headers, 'Error Response:',response.json())
    exit()

# Decode the JSON response into a dictionary and use the data
json.dumps("response.json")
data = response.json()
#print (data)

### Build a constructor to handle special character "!" in Yaml file
def general_constructor(loader, tag_suffix, node):
    return node.value
ruamel.yaml.SafeLoader.add_multi_constructor(u'!', general_constructor)

### Open Parameters yaml file template
mandatory_tags = {"apm_id": "INC0000060", "costcenter": "abcd"}
### Validate SNOW CI tags agaisnt mandatory tags defined in parameters file
#for item in data["result"]:
 ###     print("True")
    #    with open ("result1.txt", "w") as f:
     #       f.writelines(mandatory_tags["apm_id"])
      #      f.close()
    #else:
     #   print("False")

for item in data["result"]:
    number=(item.get("number"))

apm_id=(mandatory_tags["apm_id"])

def comp_str(number,apm_id):
        ch=0
        if len(number)==len(apm_id):
            for i in range (len(number)):
             if number[i]!=apm_id[i]:
                print("Strings are not equal")
                ch=1
                break
        if(ch==0):
             print("True")
             with open ("result1.txt", "w") as f:
                f.writelines(mandatory_tags["apm_id"])
                f.close()
        else:
            print("False")
comp_str(number,apm_id)
    
## Validate SNOW CI tags agaisnt mandatory tags defined in parameters file
#for item in data["result"]:
    #if item.get("number") == mandatory_tags["apm_id"]:
    #if item.get("number") == variable.apm_id:
    #    print(item.get("number"))
    #    print("True")
    #    with open ("result1.txt", "w") as f:
    #        f.writelines(variable.apm_id)
    #        f.close()
    #else:
    #    print("False")

